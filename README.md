# Multivariant assignments generation language

This library allows you to write short description of multivariant assignments in embedded DSL and interpret them as solution, text of assignment and tests.

This library is available at [hackage](http://hackage.haskell.org/package/multivariant).

## Installation

- install [stack](https://docs.haskellstack.org/en/stable/README/)
- run `stack exec multivariant`

or you can use `cabal install multivariant`.

## Language description

There are several typeclasses for tagless-final encoding of language:

- Program
- WithDescription
- WithCornerCases
- WithInvert

Each typeclass defines some operations that every interpreter has to implement.

There are currently three ``interpreters'' (types that implement corresponding typeclasses):

- Cases
- Description
- Solution

### Program

Provides following operations:

- `step` to apply bijection `f :<->: g` (actually, `g` has to be right inverse of `f`: `f . g === id`)
- `~>` to combine two circuits sequentially 
- `<***>` to combine two circuits in parallel
- `<+++>` to provide variants of circuit

### WithDescription

Provides `withDescription` operation that gives a description to circuit.

### WithCornerCases

Provides `withCornerCases` operation that allow to define corner cases to be tested in input and in output.

### WithInvert

Provides `invert` operation that inverts circuit.

## Examples

![Example task](doc/diagram.png)

```haskell
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE RankNTypes #-}
{-# LANGUAGE TypeOperators #-}

import           Data.Invertible.Bijection
import           Prelude (Integer, (+), (-), (*), ($))
import qualified Prelude as P
import           Data.Invertible.List

-- | Part alpha. Adds 5 to each list element
--
-- >  step (Inv.map $ (\x -> x+5) :<->: (\x -> x-5))
--
-- We use 'Data.Invertible.List.map' and 'Data.Invertible.Bijection.(:<->:)'
alpha :: P prog [Integer] [Integer]
alpha = step (map $ (\x -> x+5) :<->: (\x -> x-5))
        `withCornerCases` ([[],[-1,5],[5,4]],
                           [])
        `withDescription` "Add 5 to each element of the list"

-- | Part beta. Either sum or product of given list
--
-- >  step (sum :<->: (\x -> [x,0]))
--
-- 'Prelude.sum' is not invertible, so we use a (right) inverse.
beta :: P prog [Integer] Integer
beta = oneof [beta1, beta2]
  where beta1 = step (P.sum :<->: (\x -> [x,0]))
                `withCornerCases` ([[],[3,2]],
                                   [0])
                `withDescription` "Compute sum of elements of list"
        beta2 = step (P.product :<->: (\p -> [p,1]))
                `withDescription` "Compute product of elements of list"
                `withCornerCases` ([[],[0]],
                                   [])

-- | Part gamma.
--
-- >  step ((\(xs,y) -> map (*y) xs) :<->: (\ys -> (ys,1)))
--
-- We use a (right) inverse @(\ys -> (ys,1))@.
gamma :: P prog ([Integer],Integer) [Integer]
gamma = step ((\(xs,y) -> P.map (*y) xs) :<->: (\ys -> (ys,1)))
        `withCornerCases` ([ ([],1), ([1,2],0), ([],0), ([1,2],2)],
                           [ ])
        `withDescription` "Multiply each element of result of first operation to result of second operation"

-- | Part delta.
--
-- Either sum or product of list
delta :: P prog [Integer] Integer
delta = delta1 <+++> delta2
  where delta1 = step (P.product :<->: (\p -> [p,1]))
                 `withDescription` "Compute product of elements of list"
                 `withCornerCases` ([[],[0]],
                                    [])
        delta2 = step (P.sum :<->: (\s -> [s,0]))
                 `withDescription` "Compute sum of elements of list"
                 `withCornerCases` ([[]],
                                    [0])

-- | Combined task
task :: P prog Input Output
task = (alpha <***> beta) ~> gamma ~> delta
```

More examples are available in the [examples](examples/) directory.

