{-|
Module      : Test.Multivariant.Types.Description
Description : Interpreter for description
Copyright   : (c) Anton Marchenko, Mansur Ziatdinov, 2016-2017
License     : BSD-3
Maintainer  : gltronred@gmail.com
Stability   : provisional
Portability : POSIX

This module provides interpreter for description.
-}

module Test.Multivariant.Types.Description where

import Test.Multivariant.Classes

import Data.Monoid ((<>))
import Data.Text.Lazy (Text)
import Data.Text.Lazy.Builder

-- | Type for interpreter
newtype Description a b = Description { variants :: [Builder] }

-- | Get a list of texts of variants
getDescription :: Description a b -> [Text]
getDescription = map toLazyText . variants

instance Program Description where
  step _f = Description [flush]
  a ~> b = Description [da <> db | da <- variants a, db <- variants b ]
  a <***> b = Description [da <> db | da <- variants a, db <- variants b ]
  a <+++> b = Description (variants a ++ variants b)

instance WithCornerCases Description where
  withCornerCases p _ = p

instance WithDescription Description where
  withDescription (Description b) t = Description $ map ((fromLazyText t <>) . (singleton '\n' <>)) b

