{-|
Module      : Test.Multivariant.Types
Description : Intepreters for language
Copyright   : (c) Anton Marchenko, Mansur Ziatdinov, 2016-2017
License     : BSD-3
Maintainer  : gltronred@gmail.com
Stability   : provisional
Portability : POSIX

This module exports all interpreters.
-}

module Test.Multivariant.Types
  ( -- * Interpreter for corner cases
    Cases
  , getCases
    -- * Interpreter for description
  , Description
  , getDescription
    -- * Interpreter for solution
  , Solution
  , getSolutions
  ) where

import Test.Multivariant.Types.Cases
import Test.Multivariant.Types.Description
import Test.Multivariant.Types.Solution

