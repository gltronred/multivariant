{-|
Module      : Lib
Description : Example usage of library
Copyright   : (c) Anton Marchenko, Mansur Ziatdinov, 2016-2017
License     : BSD-3
Maintainer  : gltronred@gmail.com
Stability   : experimental
Portability : POSIX

This module provides 'appMain' function that prints variants for 'Task.task'

See "Task" module for more description
-}

{-# LANGUAGE OverloadedStrings #-}

module Main where

import Test.Multivariant.Types

import Task (task, inputVariants, printTask)

import Data.List (zip4)

-- | Prints all variants from 'Task.task'
appMain :: IO ()
appMain = do
  let descriptions = getDescription task
      solutions = getSolutions task
      cases = getCases task
  mapM_ (\(i,d,s,c) -> printTask i d inputVariants s c) $
    zip4 [1..] descriptions solutions cases

main = appMain


