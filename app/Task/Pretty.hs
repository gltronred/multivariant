{-|
Module      : Task.Pretty
Description : Pretty printing task elements
Copyright   : (c) Anton Marchenko, Mansur Ziatdinov, 2016-2017
License     : BSD-3
Maintainer  : gltronred@gmail.com
Stability   : experimental
Portability : POSIX

This module provides functions to pretty print variants, e.g. text description, solution,
input/output, test cases.

See "Task" module for more description
-}

{-# LANGUAGE OverloadedStrings #-}
module Task.Pretty where

import           Task.Types

import           Control.Monad
import           Data.Text.Lazy (Text)
import qualified Data.Text.Lazy as T
import           Data.Text.Lazy.Builder
import           Data.Text.Lazy.Builder.Int
import qualified Data.Text.Lazy.IO as TIO

-- | Outputs a line of given chars
hline :: Char -> IO ()
hline = TIO.putStrLn . T.replicate 80 . T.singleton

-- | Outputs a number
int :: Integral a => a -> Text
int = toLazyText . decimal

-- | Prints a list of numbers
printList :: Integral a => [a] -> IO ()
printList = TIO.putStr . T.intercalate ", " . map (toLazyText . decimal)

-- | Prints an input
printInput :: Input -> IO ()
printInput (xs,ys) = do
  TIO.putStr "["
  printList xs
  TIO.putStr "], ["
  printList ys
  -- TIO.putStr $ sh ys
  TIO.putStr "]"

-- | Outputs anything (not optimal performance, uses conversion to String)
sh :: Show a => a -> Text
sh = toLazyText . fromString . show

-- | Pretty prints a variant
printTask :: Int                -- ^ Variant number
          -> Text               -- ^ Text
          -> [Input]            -- ^ Example inputs
          -> (Input -> Output)  -- ^ Solution (it is launched on example inputs and corresponding input-output pairs are printed)
          -> [(Input, Output)]  -- ^ Test cases
          -> IO ()
printTask i varDesc varInputs varSol varTests = do
  hline '='
  TIO.putStr "Variant "
  TIO.putStrLn (int i)
  hline '-'
  TIO.putStrLn varDesc
  hline '-'
  forM_ varInputs $ \input -> do
    TIO.putStr "* "
    printInput input
    TIO.putStr "\t  ==>  \t"
    TIO.putStrLn (sh $ varSol input)
  hline '-'
  forM_ varTests $ \(input, output) -> do
    TIO.putStr "* "
    printInput input
    TIO.putStr "\t  ///  \t"
    TIO.putStr (sh output)
    TIO.putStr "\t  ==>  \t"
    TIO.putStrLn (sh $ varSol input)
  hline '='

