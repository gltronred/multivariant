{-|
Module      : Task.Types
Description : Example usage of library
Copyright   : (c) Anton Marchenko, Mansur Ziatdinov, 2016-2017
License     : BSD-3
Maintainer  : gltronred@gmail.com
Stability   : experimental
Portability : POSIX

This module provides 'Input' and 'Output' types for "Task" and "Task.Pretty"

See "Task" module for more description
-}

module Task.Types where

type Input = ([Integer], [Integer])
type Output = Integer

